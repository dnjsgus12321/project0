#include "filesys/inode.h"
#include <list.h>
#include <debug.h>
#include <round.h>
#include <string.h>
#include <stdio.h>
#include "filesys/filesys.h"
#include "filesys/free-map.h"
#include "threads/malloc.h"
#include "filesys/cache.h"

/* Identifies an inode. */
#define INODE_MAGIC 0x494e4f44
#define NUM_DIRECT_BLOCK 123
/* On-disk inode.
   Must be exactly BLOCK_SECTOR_SIZE bytes long. */
struct inode_disk
  {
    bool is_dir;                           /* True if directory, False otherwise */
    off_t length;                       /* File size in bytes. */
    unsigned magic;                     /* Magic number. */
    block_sector_t direct_block[NUM_DIRECT_BLOCK];
    block_sector_t indirect_block;
    block_sector_t double_indirect_block;
    //uint32_t unused[125];               /* Not used. */
  };

/* Returns the number of sectors to allocate for an inode SIZE
   bytes long. */
static inline size_t
bytes_to_sectors (off_t size)
{
  return DIV_ROUND_UP (size, BLOCK_SECTOR_SIZE);
}

/* In-memory inode. */
struct inode 
  {
    struct list_elem elem;              /* Element in inode list. */
    block_sector_t sector;              /* Sector number of disk location. */
    int open_cnt;                       /* Number of openers. */
    bool removed;                       /* True if deleted, false otherwise. */
    int deny_write_cnt;                 /* 0: writes ok, >0: deny writes. */
    struct inode_disk data;             /* Inode content. */
  };

/* Returns the block device sector that contains byte offset POS
   within INODE.
   Returns -1 if INODE does not contain data for a byte at offset
   POS. */
static block_sector_t
byte_to_sector (const struct inode *inode, off_t pos) 
{
  ASSERT (inode != NULL);
  uint32_t block_idx = pos / BLOCK_SECTOR_SIZE;
  block_sector_t sector_wanted;
  static char zeros[BLOCK_SECTOR_SIZE];
  block_sector_t *buf_sector = calloc(1, sizeof(block_sector_t));
  block_sector_t *buf_sector2 = calloc(1, sizeof(block_sector_t));

  if (pos >= inode->data.length)
    return -1;

  if (block_idx < NUM_DIRECT_BLOCK){
    sector_wanted = inode->data.direct_block[block_idx];

    if (sector_wanted){
      return sector_wanted;
    }
    else{
      if (free_map_allocate(1, &inode->data.direct_block[block_idx])){
        sector_wanted = inode->data.direct_block[block_idx];
        cache_write(sector_wanted, zeros, 0, BLOCK_SECTOR_SIZE);
      }
    }
  }

  else if (block_idx < NUM_DIRECT_BLOCK + BLOCK_SECTOR_SIZE/4){
    uint32_t indirect_sector_ofs = 4*(block_idx - NUM_DIRECT_BLOCK);
    
    if (!inode->data.indirect_block){
      if (free_map_allocate(1, &inode->data.indirect_block)){
        cache_write(inode->data.indirect_block, zeros, 0, BLOCK_SECTOR_SIZE);
      }
    }
    cache_read(inode->data.indirect_block, buf_sector, indirect_sector_ofs, 4);

    if (!*buf_sector){
      if (free_map_allocate(1, buf_sector)){
        cache_write(inode->data.indirect_block, buf_sector, indirect_sector_ofs, 4);
        cache_write(*buf_sector, zeros, 0, BLOCK_SECTOR_SIZE);
      }
    }
    sector_wanted = *buf_sector;
  }

  else if (block_idx < NUM_DIRECT_BLOCK + BLOCK_SECTOR_SIZE/4*(BLOCK_SECTOR_SIZE/4 + 1)){
    uint32_t double_indirect_idx = block_idx - NUM_DIRECT_BLOCK - BLOCK_SECTOR_SIZE/4;
    
    if (!inode->data.double_indirect_block){
      if (free_map_allocate(1, &inode->data.double_indirect_block)){
        cache_write(inode->data.double_indirect_block, zeros, 0, BLOCK_SECTOR_SIZE);
      }
    }
    cache_read(inode->data.double_indirect_block, buf_sector, double_indirect_idx/128*4, 4);
    if (!*buf_sector){
      if (free_map_allocate(1, buf_sector)){
        cache_write(inode->data.double_indirect_block, buf_sector, double_indirect_idx/128*4, 4);
        cache_write(*buf_sector, zeros, 0, BLOCK_SECTOR_SIZE);
      }
    }
    cache_read(*buf_sector, buf_sector2, (double_indirect_idx%128)*4, 4);

    if (!*buf_sector2){
      if (free_map_allocate(1, buf_sector2)){
        cache_write(*buf_sector, buf_sector2, (double_indirect_idx%128)*4, 4);
        cache_write(*buf_sector2, zeros, 0, BLOCK_SECTOR_SIZE);
      }
    }
    sector_wanted = *buf_sector2;

  }

  else{
    printf("can't extend file anymore\n");
    free(buf_sector);
    free(buf_sector2);
    return -1;
  }
  
  free(buf_sector);
  free(buf_sector2);
  return sector_wanted;
}


/* List of open inodes, so that opening a single inode twice
   returns the same `struct inode'. */
static struct list open_inodes;

/* Initializes the inode module. */
void
inode_init (void) 
{
  list_init (&open_inodes);
}

/* Initializes an inode with LENGTH bytes of data and
   writes the new inode to sector SECTOR on the file system
   device.
   Returns true if successful.
   Returns false if memory or disk allocation fails. */
bool
inode_create (block_sector_t sector, off_t length, bool is_dir)
{
  struct inode_disk *disk_inode = NULL;
  bool success = false;
  //printf("\n\nlength when creating %d\n\n", length);
  ASSERT (length >= 0);
  
  /* If this assertion fails, the inode structure is not exactly
     one sector in size, and you should fix that. */
  ASSERT (sizeof *disk_inode == BLOCK_SECTOR_SIZE);
  disk_inode = calloc (1, sizeof *disk_inode);
  if (disk_inode != NULL)
    {
      size_t total_sectors = bytes_to_sectors (length);
      size_t direct_sectors = 0;
      size_t indirect_sectors = 0;
      size_t double_indirect_sectors = 0;
      block_sector_t *buf_sector = calloc(1, sizeof(block_sector_t));
      block_sector_t *buf_sector2 = calloc(1, sizeof(block_sector_t));
      if (total_sectors <= NUM_DIRECT_BLOCK)
        direct_sectors = total_sectors;
      else if(total_sectors <= NUM_DIRECT_BLOCK + BLOCK_SECTOR_SIZE/4){
        direct_sectors = NUM_DIRECT_BLOCK;
        indirect_sectors = total_sectors - NUM_DIRECT_BLOCK;
      }
      else if(total_sectors <= NUM_DIRECT_BLOCK + BLOCK_SECTOR_SIZE/4*(BLOCK_SECTOR_SIZE/4 + 1)){
        direct_sectors = NUM_DIRECT_BLOCK;
        indirect_sectors = BLOCK_SECTOR_SIZE/4;
        double_indirect_sectors = total_sectors - direct_sectors - indirect_sectors;
      }
      disk_inode->length = length;
      disk_inode->magic = INODE_MAGIC;
      disk_inode->is_dir = is_dir;
      memset(disk_inode->direct_block, 0, sizeof(block_sector_t)*NUM_DIRECT_BLOCK);
      memset(&disk_inode->indirect_block, 0, sizeof(block_sector_t));
      memset(&disk_inode->double_indirect_block, 0, sizeof(block_sector_t));
      //cache_write(sector, disk_inode, 0, BLOCK_SECTOR_SIZE);
      static char zeros[BLOCK_SECTOR_SIZE];
      size_t i;
      for (i = 0; i < direct_sectors; i++){
        if (free_map_allocate (1, &disk_inode->direct_block[i])){
    cache_write(disk_inode->direct_block[i], zeros, 0, BLOCK_SECTOR_SIZE);
        }
      }

      for (i = 0; i < indirect_sectors; i++){
        if (i == 0){
          if(free_map_allocate(1, &disk_inode->indirect_block)){
            cache_write(disk_inode->indirect_block, zeros, 0, BLOCK_SECTOR_SIZE);
          }
        }
        if (free_map_allocate(1, buf_sector)){
          cache_write(disk_inode->indirect_block, buf_sector, i*sizeof(block_sector_t), 4);
          cache_write(*buf_sector, zeros, 0, BLOCK_SECTOR_SIZE);
        }
      }

      for(i = 0; i < double_indirect_sectors; i ++){
        if (i == 0){
          if (free_map_allocate(1, &disk_inode->double_indirect_block)){
            cache_write(disk_inode->double_indirect_block, zeros, 0, BLOCK_SECTOR_SIZE);
          }
        }
        cache_read(disk_inode->double_indirect_block, buf_sector, i/128*4, 4);
        if (!*buf_sector){
          if (free_map_allocate(1, buf_sector)){
            cache_write(disk_inode->double_indirect_block, buf_sector, i/128*4, 4);
            cache_write(*buf_sector, zeros, 0, BLOCK_SECTOR_SIZE);
          }
        }
        if (free_map_allocate(1, buf_sector2)){
          cache_write(*buf_sector, buf_sector2, (i%128)*4, 4);
          cache_write(*buf_sector2, zeros, 0, BLOCK_SECTOR_SIZE);
        }
      }
      free(buf_sector);
      free(buf_sector2);
      cache_write(sector, disk_inode, 0, BLOCK_SECTOR_SIZE);
      success = true;
      /*    
      if (free_map_allocate (sectors, &disk_inode->start)) 
        {
          cache_write(sector, disk_inode, 0, BLOCK_SECTOR_SIZE);
          if (sectors > 0) 
            {
              static char zeros[BLOCK_SECTOR_SIZE];
              size_t i;
              
              for (i = 0; i < sectors; i++) 
                cache_write(disk_inode->start + i, zeros, 0, BLOCK_SECTOR_SIZE);
            }
          success = true; 
        } 
      free (disk_inode);
      */
      free (disk_inode);
    }
  return success;
}

/* Reads an inode from SECTOR
   and returns a `struct inode' that contains it.
   Returns a null pointer if memory allocation fails. */
struct inode *
inode_open (block_sector_t sector)
{
  struct list_elem *e;
  struct inode *inode;

  /* Check whether this inode is already open. */
  for (e = list_begin (&open_inodes); e != list_end (&open_inodes);
       e = list_next (e)) 
    {
      inode = list_entry (e, struct inode, elem);
      if (inode->sector == sector) 
        {
          inode_reopen (inode);
          return inode; 
        }
    }

  /* Allocate memory. */
  inode = malloc (sizeof *inode);
  if (inode == NULL)
    return NULL;

  /* Initialize. */
  list_push_front (&open_inodes, &inode->elem);
  inode->sector = sector;
  inode->open_cnt = 1;
  inode->deny_write_cnt = 0;
  inode->removed = false;
  //block_read (fs_device, inode->sector, &inode->data);
  cache_read(sector, &inode->data, 0, BLOCK_SECTOR_SIZE);
  return inode;
}

/* Reopens and returns INODE. */
struct inode *
inode_reopen (struct inode *inode)
{
  if (inode != NULL)
    inode->open_cnt++;
  return inode;
}

/* Returns INODE's inode number. */
block_sector_t
inode_get_inumber (const struct inode *inode)
{
  return inode->sector;
}

/* Closes INODE and writes it to disk.
   If this was the last reference to INODE, frees its memory.
   If INODE was also a removed inode, frees its blocks. */
void
inode_close (struct inode *inode) 
{
  int i;
  /* Ignore null pointer. */
  if (inode == NULL)
    return;

  /* Release resources if this was the last opener. */
  if (--inode->open_cnt == 0)
    {
      /* Remove from inode list and release lock. */
      list_remove (&inode->elem);
 
      /* Deallocate blocks if removed. */
      if (inode->removed) 
        {
          free_map_release (inode->sector, 1);
          for (i=0; i<NUM_DIRECT_BLOCK; i++){
            if (inode->data.direct_block[i])
              free_map_release(inode->data.direct_block[i], 1);
          }

          if (inode->data.indirect_block){
            block_sector_t *buf = calloc(1, BLOCK_SECTOR_SIZE);
            cache_read(inode->data.indirect_block, buf, 0, BLOCK_SECTOR_SIZE);
            for (i=0; i<128; i++){
              if (*(buf+i))
                free_map_release(*(buf+i), 1);
            }
            free(buf);
            free_map_release(inode->data.indirect_block, 1);
          }

          if (inode->data.double_indirect_block){
            int ii;
            block_sector_t *buf = calloc(1, BLOCK_SECTOR_SIZE);
            block_sector_t *buf2 = calloc(1, BLOCK_SECTOR_SIZE);
            cache_read(inode->data.double_indirect_block, buf, 0, BLOCK_SECTOR_SIZE);
            for (i=0; i<128; i++){
              if (*(buf+i)){
                cache_read(*(buf+i), buf2, 0, BLOCK_SECTOR_SIZE);
                for (ii=0; ii<128; ii++){
                  if (*(buf2+ii)){
                    free_map_release(*(buf2+ii), 1);
                  }
                }
                free_map_release(*(buf+i), 1);
              }
            } 
            free_map_release(inode->data.double_indirect_block, 1);
            free(buf);
            free(buf2);
          }
            
         
          //free_map_release (inode->data.start,
          //                  bytes_to_sectors (inode->data.length)); 
        }

      free (inode); 
    }
}

/* Marks INODE to be deleted when it is closed by the last caller who
   has it open. */
void
inode_remove (struct inode *inode) 
{
  ASSERT (inode != NULL);
  inode->removed = true;
}

/* Reads SIZE bytes from INODE into BUFFER, starting at position OFFSET.
   Returns the number of bytes actually read, which may be less
   than SIZE if an error occurs or end of file is reached. */
off_t
inode_read_at (struct inode *inode, void *buffer_, off_t size, off_t offset) 
{
  uint8_t *buffer = buffer_;
  off_t bytes_read = 0;
  uint8_t *bounce = NULL;

  while (size > 0) 
    {
      /* Disk sector to read, starting byte offset within sector. */
      block_sector_t sector_idx = byte_to_sector (inode, offset);
      //printf("sec_idx in inoderead:%d\n", sector_idx);
      //printf("READ inode_sector: length: %d, read_end: %d \n", inode->data.length, offset+size);
      int sector_ofs = offset % BLOCK_SECTOR_SIZE;

      /* Bytes left in inode, bytes left in sector, lesser of the two. */
      off_t inode_left = inode_length (inode) - offset;
      int sector_left = BLOCK_SECTOR_SIZE - sector_ofs;
      int min_left = inode_left < sector_left ? inode_left : sector_left;


      /* Number of bytes to actually copy out of this sector. */
      int chunk_size = size < min_left ? size : min_left;
      if (chunk_size <= 0)
        break;


      cache_read(sector_idx, buffer + bytes_read, sector_ofs, chunk_size);
      /*if (sector_ofs == 0 && chunk_size == BLOCK_SECTOR_SIZE)
        {

          //block_read (fs_device, sector_idx, buffer + bytes_read);
          cache_read(sector_idx, buffer + bytes_read);
        }
      else 
        {

          if (bounce == NULL) 
            {
              bounce = malloc (BLOCK_SECTOR_SIZE);
              if (bounce == NULL)
                break;
            }
          //block_read (fs_device, sector_idx, bounce);
          cache_read(sector_idx, bounce);  
          memcpy (buffer + bytes_read, bounce + sector_ofs, chunk_size);
        }*/
      
      /* Advance. */
      size -= chunk_size;
      offset += chunk_size;
      bytes_read += chunk_size;
    }
  free (bounce);

  return bytes_read;
}

/* Writes SIZE bytes from BUFFER into INODE, starting at OFFSET.
   Returns the number of bytes actually written, which may be
   less than SIZE if end of file is reached or an error occurs.
   (Normally a write at end of file would extend the inode, but
   growth is not yet implemented.) */
off_t
inode_write_at (struct inode *inode, const void *buffer_, off_t size,
                off_t offset) 
{
  const uint8_t *buffer = buffer_;
  off_t bytes_written = 0;
  uint8_t *bounce = NULL;

  if (inode->deny_write_cnt)
    return 0;
  if (inode->data.length < offset + size)
    inode->data.length = offset + size;
  cache_write(inode->sector, &inode->data, 0, BLOCK_SECTOR_SIZE);
  //printf("WRITE length: %d, write_end: %d \n", inode->data.length, offset+size);
  while (size > 0) 
    {
      
      /* Sector to write, starting byte offset within sector. */
      block_sector_t sector_idx = byte_to_sector (inode, offset);
      cache_write(inode->sector, &inode->data, 0, BLOCK_SECTOR_SIZE);
      int sector_ofs = offset % BLOCK_SECTOR_SIZE;

      /* Bytes left in inode, bytes left in sector, lesser of the two. */
      off_t inode_left = inode_length (inode) - offset;
      int sector_left = BLOCK_SECTOR_SIZE - sector_ofs;
      int min_left = inode_left < sector_left ? inode_left : sector_left;

      /* Number of bytes to actually write into this sector. */
      int chunk_size = size < min_left ? size : min_left;
      if (chunk_size <= 0)
        break;


      cache_write(sector_idx, buffer + bytes_written, sector_ofs, chunk_size);
      /*if (sector_ofs == 0 && chunk_size == BLOCK_SECTOR_SIZE)
        {
          //block_write (fs_device, sector_idx, buffer + bytes_written);
          cache_write(sector_idx, buffer + bytes_written);
        }
      else 
        {
          if (bounce == NULL) 
            {
              bounce = malloc (BLOCK_SECTOR_SIZE);
              if (bounce == NULL)
                break;
            }

          if (sector_ofs > 0 || chunk_size < sector_left) 
            cache_read (sector_idx, bounce);
          else
            memset (bounce, 0, BLOCK_SECTOR_SIZE);
          memcpy (bounce + sector_ofs, buffer + bytes_written, chunk_size);
          cache_write(sector_idx, bounce);
        }*/

      /* Advance. */
      size -= chunk_size;
      offset += chunk_size;
      bytes_written += chunk_size;
    }
  free (bounce);

  return bytes_written;
}

/* Disables writes to INODE.
   May be called at most once per inode opener. */
void
inode_deny_write (struct inode *inode) 
{
  inode->deny_write_cnt++;
  ASSERT (inode->deny_write_cnt <= inode->open_cnt);
}

/* Re-enables writes to INODE.
   Must be called once by each inode opener who has called
   inode_deny_write() on the inode, before closing the inode. */
void
inode_allow_write (struct inode *inode) 
{
  ASSERT (inode->deny_write_cnt > 0);
  ASSERT (inode->deny_write_cnt <= inode->open_cnt);
  inode->deny_write_cnt--;
}

/* Returns the length, in bytes, of INODE's data. */
off_t
inode_length (const struct inode *inode)
{
  return inode->data.length;
}

bool
inode_is_dir(struct inode *inode)
{
  return inode->data.is_dir;
}

bool
inode_is_removed(struct inode *inode)
{
  return inode->removed;
}

