#ifndef VM_PAGE_H
#define VM_PAGE_H

#include <hash.h>
#include "filesys/off_t.h"

enum p_state
  {
      FROM_FILE,   /* From filesys, ready to be loaded into physical frame */
      IN_SWAP,
      IN_RAM
  };

struct STE
{
    void *uaddr;// user virtual address of the corresponding page
    void *paddr;
    enum p_state page_state; //page state
    bool writable;
    bool present;

    struct file* file;
    off_t offset;
    uint32_t read_bytes;
    uint32_t zero_bytes;

    uint32_t swap_index;
    struct hash_elem hash_elem; 
};

struct hash* ST_create(void);
uint32_t ST_hash_func(struct hash_elem *h_ele, void *aux UNUSED);
bool ST_hash_less(struct hash_elem *a, struct hash_elem *b, void *aux UNUSED);
void insert_STE_for_stack(struct hash* ST, void* uaddr, uint8_t* kpage);
bool insert_STE_for_file(struct file* file, struct hash* ST, void* uaddr, off_t ofs, uint32_t read_bytes, uint32_t zero_bytes, bool writable);
bool unmap_STE(struct hash* ST, uint32_t *pagedir, void *vaddr, struct file *file, uint32_t file_bytes, int offset);
struct STE* find_ste(struct hash* ST, void* uaddr); 
bool ST_update_swap_out(struct hash* ST, void* uaddr, uint32_t swap_index);
void ST_free(struct hash* ST);

#endif
