#include <stdio.h> 
#include <string.h>
#include <hash.h>
#include "vm/page.h"
#include "threads/synch.h" 
#include "threads/palloc.h" 
#include "threads/malloc.h" 
#include "threads/thread.h" 
#include "userprog/pagedir.h" 
#include "userprog/syscall.h"
#include "userprog/process.h"
#include "userprog/pagedir.h"
#include "vm/frame.h"
#include "vm/swap.h"
#include "filesys/file.h"


struct hash* 
ST_create(void)
{
  struct hash* ST = (struct hash*)malloc(sizeof(struct hash));
  hash_init(ST, ST_hash_func, ST_hash_less, NULL);
  return ST;
}

/*
void ST_init(struct hash* ST)
{
  hash_init(ST, ST_hash_func, ST_hash_less, NULL);
}
*/
uint32_t
ST_hash_func(struct hash_elem *h_ele, void *aux UNUSED)
{
  struct STE* ST_entry = hash_entry(h_ele, struct STE, hash_elem);
  return hash_int((int) ST_entry->uaddr);
}

bool
ST_hash_less(struct hash_elem *a, struct hash_elem *b, void *aux UNUSED)
{
  struct STE* ste_a = hash_entry(a, struct STE, hash_elem);
  struct STE* ste_b = hash_entry(b, struct STE, hash_elem);
  return(ste_a->uaddr < ste_b->uaddr);
}

void
ST_destructor(struct hash_elem* elem)
{
  struct STE* ste = hash_entry(elem, struct STE, hash_elem);
  if(ste->page_state == IN_RAM)
  {
    remove_frame(ste->paddr);
  }
  else if(ste->page_state == IN_SWAP)
  {
    swap_free(ste->swap_index);
  }
  free(ste);
}

void
insert_STE_for_stack(struct hash* ST, void * uaddr, uint8_t *kpage)
{
  struct STE *ste = (struct STE*)malloc(sizeof(struct STE));
  ste->uaddr = uaddr;
  ste->paddr = kpage;
  ste->writable = true;
  ste->page_state = IN_RAM;
  ste->present = true;
  hash_insert(ST, &ste->hash_elem);
}
 
bool
insert_STE_for_file(struct file* file, struct hash* ST, void * uaddr,  off_t ofs, uint32_t read_bytes, uint32_t zero_bytes, bool writable)
{
  struct STE *ste = (struct STE*)malloc(sizeof(struct STE));
  ste->uaddr = uaddr;
  ste->paddr = NULL;
  ste->page_state = FROM_FILE;
  
  ste->file = file;
  ste->offset = ofs;
  ste->read_bytes = read_bytes;
  ste->zero_bytes = zero_bytes;

  ste->writable = writable;
  ste->present = false;

  return !hash_insert(ST, &ste->hash_elem);
}

bool
unmap_STE(struct hash* ST, uint32_t *pagedir, void *vaddr, struct file *file,
          uint32_t file_bytes, int offset)
{
  /* 0. Find corrosponding ste */
  struct STE *ste = find_ste(ST, vaddr);
  if(ste == NULL)
    return false;

  /* 1. If ste is IN_RAM */
  if(ste->page_state == IN_RAM)
  {
    pin_or_unpin(ste->paddr, true);
    // If it is dirty, it should be written back to filesys 
    // Since lock is already acquired at userprog/syscall.c, 
    // we don't need to acquire any locks here
    if(pagedir_is_dirty(pagedir, ste->uaddr) || pagedir_is_dirty(pagedir, ste->paddr))
    {
      file_write_at(file, ste->uaddr, file_bytes, offset);
    }
    remove_frame(ste->paddr);  // We should use this because we don't acquire frame lock 
                               // in free_freme()
    palloc_free_page(ste->paddr);
    pagedir_clear_page(pagedir, ste->uaddr);
  }
  /* 2. If ste is IN_SWAP */
  else if(ste->page_state == IN_SWAP)
  {
    void *temp;
    if(pagedir_is_dirty(pagedir, ste->uaddr)) // We can't check for paddr in here
    {
      temp = palloc_get_page (PAL_ZERO);      // In case of user pool is full, 
                                              // we get this page from kernel pool
      swap_in(temp, ste->swap_index);
      file_write_at(file, temp, file_bytes, offset);
      palloc_free_page(temp);
    }
    else
      swap_free(ste->swap_index);
  }
  /* 3. If ste is FROM_FILE, there is nothing to do */
  /* 4. Final : remove ste from ST */
  hash_delete(ST, &ste->hash_elem);
  free(ste);
  return true;
}
/*
bool
load_page_for_file(struct STE* ste)
{
  //assert(ste->page_state == FROM_FILE);
  lock_acquire(&frame_lock);
  uint8_t *kpage = allocate_frame(thread_current(), ste->uaddr, false);
  if (kpage == NULL){
    lock_release(&frame_lock);
    return false;
  }
  lock_acquire(&ste->file->filesys_lock);

  if (file_read_at(ste->file, kpage, ste->read_bytes, ste->offset) != 
    (int) ste->read_bytes)
  {
    free_frame(kpage);
    lock_release(&ste->file->filesys_lock);
    lock_release(&frame_lock);
    return false;
  }
  lock_release(&ste->file->filesys_lock);
  memset(kpage+ste->read_bytes, 0, ste->zero_bytes);
  
  if (!pagedir_set_page(thread_current()->pagedir ,ste->uaddr, kpage, ste->writable))
  {
    free_frame(kpage);
    lock_release(&frame_lock);
    return false;
  }
  ste->paddr = kpage;
  ste->present = true;
  ste->page_state = IN_RAM;
  lock_release(&frame_lock);
  return true;
}
*/
/*
bool
load_page_for_file(struct STE* ste)
{
  //assert(ste->page_state == FROM_FILE);
  uint8_t *kpage = allocate_frame(thread_current(), ste->uaddr, false);
  if (kpage == NULL)
    return false;
  
  lock_acquire(&ste->file->filesys_lock);

  if (file_read_at(ste->file, kpage, ste->read_bytes, ste->offset) != 
    (int) ste->read_bytes)
  {
    free_frame(kpage);
    lock_release(&ste->file->filesys_lock);
    return false;
  }
  lock_release(&ste->file->filesys_lock);
  memset(kpage+ste->read_bytes, 0, ste->zero_bytes);
  
  if (!pagedir_set_page(thread_current()->pagedir ,ste->uaddr, kpage, ste->writable))
  {
    free_frame(kpage);
    return false;
  }
  ste->paddr = kpage;
  ste->present = true;
  ste->page_state = IN_RAM;
  return true;
}
*/

struct STE*
find_ste(struct hash* ST, void* uaddr)
{
  struct STE temp_ste;
  temp_ste.uaddr = uaddr;
  struct hash_elem* elem = hash_find(ST, &temp_ste.hash_elem);
  if(elem == NULL)
    return NULL;
  else
    return hash_entry(elem, struct STE, hash_elem);
}

bool
ST_update_swap_out(struct hash* ST, void* uaddr, uint32_t swap_index)
{
  struct STE* ste = find_ste(ST, uaddr);
  if(ste == NULL || ste->present == false)
  { 
    return false;
  }
  ste->present = false;
  ste->swap_index = swap_index;
  ste->page_state = IN_SWAP;
  ste->paddr = NULL;
  return true;
}

void ST_free(struct hash* ST)
{
  hash_destroy(ST, ST_destructor);
  free(ST);
}

