#include "filesys/filesys.h"
#include <debug.h>
#include <stdio.h>
#include <string.h>
#include "filesys/file.h"
#include "filesys/free-map.h"
#include "filesys/inode.h"
#include "filesys/directory.h"
#include "filesys/cache.h"
#include "threads/thread.h"

/* Partition that contains the file system. */
struct block *fs_device;

static void do_format (void);
static void parse_path(char *path, char *dir, char *file);

/* Initializes the file system module.
   If FORMAT is true, reformats the file system. */
void
filesys_init (bool format) 
{
  fs_device = block_get_role (BLOCK_FILESYS);
  if (fs_device == NULL)
    PANIC ("No file system device found, can't initialize file system.");

  inode_init ();
  free_map_init ();
  cache_init();

  if (format) 
    do_format ();

  free_map_open ();

  thread_current()->working_directory = dir_open_root();

}

/* Shuts down the file system module, writing any unwritten data
   to disk. */
void
filesys_done (void) 
{
  free_map_close ();
  cache_finish();
}

/* Creates a file named NAME with the given INITIAL_SIZE.
   Returns true if successful, false otherwise.
   Fails if a file named NAME already exists,
   or if internal memory allocation fails. */
bool
filesys_create (const char *path, off_t initial_size, bool is_dir) 
{
  block_sector_t inode_sector = 0;
  char directory[MAX_PATH_NAME + 1];
  char file[MAX_PATH_NAME + 1];
  struct dir *dir;
  struct dir *new_dir;

  parse_path(path, directory, file);
  dir = dir_open_path(directory);

  bool success = (dir != NULL
                  && free_map_allocate (1, &inode_sector)
                  && inode_create (inode_sector, initial_size, is_dir)
                  && dir_add (dir, file, inode_sector, is_dir));
  if (!success && inode_sector != 0) 
    free_map_release (inode_sector, 1);

  dir_close (dir);

  return success;
}

/* Opens the file with the given NAME.
   Returns the new file if successful or a null pointer
   otherwise.
   Fails if no file named NAME exists,
   or if an internal memory allocation fails. */
struct file *
filesys_open (const char *path)
{
  if (strlen(path) == 0)
    return NULL;
  char directory[MAX_PATH_NAME + 1];
  char file[MAX_PATH_NAME + 1];
  struct inode *inode = NULL;
  struct dir *dir;
  struct file *opened_file;
  //printf("path: %s\n", path);
  parse_path(path, directory, file);
  dir = dir_open_path(directory);
  
  //printf("dir: %s\n", directory);
  //printf("file: %s\n", file);
  if (dir == NULL)
    return NULL;

  /*if(!dir_lookup(dir, file, &inode))
  {
    dir_close(dir);
    return NULL;
  }*/

  if(strlen(file) > 0)
  {
    dir_lookup(dir, file, &inode);
    dir_close(dir);
  }
  else
    inode = dir_get_inode(dir);

  if(!inode || inode_is_removed(inode))
    return NULL;
  

  opened_file = file_open(inode);
  //opened_file->directory = dir;

  return opened_file;
}




/* Deletes the file named NAME.
   Returns true if successful, false on failure.
   Fails if no file named NAME exists,
   or if an internal memory allocation fails. */
bool
filesys_remove (const char *path) 
{
  char directory[MAX_PATH_NAME + 1];
  char file[MAX_PATH_NAME + 1];
  struct dir *dir;
  bool success;

  parse_path(path, directory, file);
  dir = dir_open_path(directory);
  success = (dir != NULL && dir_remove(dir, file));


  dir_close(dir);

  return success;
}

/* Formats the file system. */
static void
do_format (void)
{
  printf ("Formatting file system...");
  free_map_create ();
  if (!dir_create (ROOT_DIR_SECTOR, 16))
    PANIC ("root directory creation failed");
  free_map_close ();
  printf ("done.\n");
}

static void
parse_path(char *path, char *dir, char *file)
{
  char temp[MAX_PATH_NAME + 1];
  char *token, *remainder, *next_token;
  char *tokenizer = "/";
  int i;

  if(!path || !strlen(path))
    return;

  memset(temp, 0, MAX_PATH_NAME + 1);
  memset(dir, 0, MAX_PATH_NAME + 1);
  memset(file, 0, MAX_FILE_NAME + 1);

  //Since path is const, we need to copy it to use */
  strlcpy(temp, path, MAX_PATH_NAME);

  if(!strlen(temp))
    return;

  if(*temp == '/')
  {
    *dir = '/';
    dir++;
  }


  token = strtok_r(temp, tokenizer, &remainder);

  next_token = "";


  for(; token != NULL; token = strtok_r(NULL, tokenizer, &remainder))
  {
    i = strlen(next_token);
    if(i > 0)
    {
      memcpy(dir, next_token, i);
      dir[i] = '/';
      dir += (i + 1);
    }
    next_token = token;
  }

  *dir = '\0';
  memcpy(file, next_token, strlen(next_token) + 1);
}


bool filesys_chdir(char *dir)
{
  struct dir *new_dir = dir_open_path(dir);

  if(!new_dir)
    return false;

  dir_close(thread_current()->working_directory);
  thread_current()->working_directory = new_dir;
  return true;
}

/*bool filesys_readdir(int file_d, char* name)
{
  struct file *file;
  struct inode *inode;
  struct dir *dir;
  int i;
  bool success = true;
  off_t *pos = (off_t *)file + 1;

  if((file = thread_current()->fd[file_d]) == NULL)
    return false;

  inode = file_get_inode(file);

  if(!inode || inode_is_dir(inode))
    return false;

  if((dir = dir_open(inode)) == NULL)
  return false;

  for(i = 0; i <= *pos && success; i++)
    success = dir_readdir(dir, name);

  if(i <= *pos == false)
    *(pos)++;

  return success;  
}*/
