#ifndef VM_SWAP_H
#define VM_SWAP_H


void swap_init(void);
uint32_t swap_out(void* kpage);
void swap_in(void* kpage, uint32_t swap_index);
void swap_free(uint32_t swap_index);

#endif
