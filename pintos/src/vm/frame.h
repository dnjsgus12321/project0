#ifndef VM_FRAME_H
#define VM_FRAME_H

#include <hash.h>
#include "threads/thread.h"
#include "vm/page.h"
#include "threads/vaddr.h"

struct FTE
{
	void *paddr; // physical address of the frame
	void *uaddr; // virtual address of the corrosponding page
	struct thread *fthread; // the thread that using this frame
    struct list_elem list_elem;
    bool pin;  // prevent the page to be evicted while it is acquiring some resources
};

void frame_init(void);
uint32_t frame_hash_func(struct hash_elem *h_ele, void *aux UNUSED);
bool frame_hash_less(struct hash_elem *a, struct hash_elem *b, void *aux UNUSED);
void *allocate_frame(struct thread* cur, void *uaddr, bool PAL_ZERO_);
void free_frame(void *kpage);
void remove_frame(void *kpage);   /* Just remove FTE from the frame_table, not palloc_free() it */
bool load_page_from_swap_disk(void* fault_page, struct STE* ste);
bool load_page_for_file(struct STE* ste);
bool load_page_for_stack_growth(void* fault_page);
bool load_page_for_setup_stack(void);
struct FTE* victim_frame(void);
void pin_or_unpin(void* kpage, bool pin);

#endif
