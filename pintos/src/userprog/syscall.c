#include "userprog/syscall.h"
#include <stdio.h>
#include <syscall-nr.h>
#include <string.h>
#include <stdlib.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "threads/vaddr.h"
#include "threads/synch.h"
#include "filesys/directory.h"
#include "filesys/file.h"
#include "filesys/filesys.h"
#include "devices/input.h"
#include "devices/shutdown.h"
#include "userprog/process.h"
#include "userprog/pagedir.h"
#include "vm/page.h"
#include "filesys/inode.h"

static void syscall_handler (struct intr_frame *);
static int get_user (const uint8_t *uaddr);
bool mapid_less(struct list_elem *a, struct list_elem *b, void *aux);
void check_uaddr_validity(uint8_t *esp_, int size);
void push2list_per_byte (int i, uint32_t argv_addr, uint8_t addbyte_list[]);
typedef int pid_t;
struct semaphore syscall_lock;   /* To ensure the synchronization during the execution of syscall */


bool mapid_less(struct list_elem *a, struct list_elem *b, void *aux)
{
  struct mfile *first = list_entry (a, struct mfile, list_elem);
  struct mfile *second = list_entry (b, struct mfile, list_elem);
  return first->mapid < second->mapid;
}

static int
get_user (const uint8_t *uaddr)
{
  int result;
  asm ("movl $1f, %0; movzbl %1, %0; 1:"
       : "=&a" (result) : "m" (*uaddr));
  return result;
}


void
syscall_init (void) 
{
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
  sema_init(&syscall_lock, 1);
}

void
check_uaddr_validity(uint8_t *esp_, int size)
{ 
  bool is_buffer = size == -1;
  char c;
  struct thread *cur = thread_current ();
  uint32_t *pd;
  pd = cur->pagedir;
  for (int i=0; i<size || is_buffer; i++){
      if (is_user_vaddr(esp_+i) == false){
          cur->exit_status = -1;
          thread_exit();
      }
      if (get_user(esp_+i) == -1){
          cur->exit_status = -1;
          thread_exit();
      }
      if (is_buffer){
         c =  (char) *(esp_+i);
         if (c == '\0'){
             break;
         }
      }
  }
}

static void
syscall_handler (struct intr_frame *f UNUSED) 
{
  uint8_t *esp_ = f->esp;
  thread_current()->user_esp = f->esp;
  check_uaddr_validity(esp_, 4);
  uint32_t syscall_num = * (uint32_t *) esp_;
  esp_+=4;

  //sema_down(&syscall_lock);        /* Only one thread can access to the region below at a time */
  
  if (syscall_num == SYS_HALT){
      shutdown_power_off();
  }    
  else if (syscall_num == SYS_EXIT){
      check_uaddr_validity(esp_, 4);
      int status = *(int *) esp_;
      thread_current()->exit_status = status;
      thread_exit();
  }
  else if (syscall_num == SYS_EXEC){
      check_uaddr_validity(esp_, 4);
      char* cmd_line = (char *)*(uint32_t *)esp_;
      check_uaddr_validity((uint8_t *) cmd_line, -1);
      sema_down(&syscall_lock);
      f->eax = process_execute(cmd_line);
      sema_up(&syscall_lock);
  }
  else if (syscall_num == SYS_WAIT){
      check_uaddr_validity(esp_, 4);
      pid_t pid = *(pid_t *) esp_;
      f->eax = process_wait(pid);
  }  
  else if (syscall_num ==  SYS_CREATE){
      check_uaddr_validity(esp_, 4);
      char* file = (char *)*(uint32_t *)esp_;
      check_uaddr_validity((uint8_t *) file, -1);
      esp_+=4;
      check_uaddr_validity(esp_, 4);
      unsigned initial_size = *(unsigned *)esp_;
      sema_down(&syscall_lock);
      //printf("sts create sig in syscall: %d\n", initial_size);
      f->eax = filesys_create(file, initial_size, false);
      sema_up(&syscall_lock);
  }
  else if (syscall_num ==  SYS_REMOVE){
      check_uaddr_validity(esp_, 4);
      char* file = (char *)*(uint32_t *)esp_;
      check_uaddr_validity((uint8_t *) file, -1);
      sema_down(&syscall_lock);
      f->eax = filesys_remove(file);
      sema_up(&syscall_lock);
  }
  else if (syscall_num ==  SYS_OPEN){
      check_uaddr_validity(esp_, 4);
      char* file = (char *)*(uint32_t *)esp_;
      check_uaddr_validity((uint8_t *) file, -1);
      sema_down(&syscall_lock);
      struct file* opened_file = filesys_open(file);
      struct inode *inode;

      if(opened_file)
      {
        inode = file_get_inode(opened_file);
        if(inode && inode_is_dir(inode))
          opened_file->directory = dir_open(inode_reopen(inode));
        else
          opened_file->directory = NULL;

        for(int i=2; i < 128; i++)
        {
          if(!thread_current()->fd[i])
          {
            thread_current()->fd[i] = opened_file;
            if(!strcmp(thread_current()->name, file))
              file_deny_write(opened_file);
            f->eax = i;
            sema_up(&syscall_lock);      /* Allow for other thread to execute syscall */
            return;
          }
        }
      }

      f->eax = -1;
      sema_up(&syscall_lock); 
  }
  else if (syscall_num ==  SYS_FILESIZE){
      check_uaddr_validity(esp_, 4);
      sema_down(&syscall_lock);
      int file = *(int *) esp_;
      if(file < 2 || file > 127)
        f->eax = -1;
      else
        if(!thread_current()->fd[file])
          f->eax = -1;
        else
          f->eax = file_length(thread_current()->fd[file]);
      sema_up(&syscall_lock);
  }
  else if (syscall_num ==  SYS_READ){
      check_uaddr_validity(esp_, 4);
      int file = *(int *) esp_;
      esp_+=4;

      check_uaddr_validity(esp_, 4);
      void* buffer = (void *)*(uint32_t *)esp_;
      esp_+=4;

      check_uaddr_validity(esp_, 4);
      unsigned length = *(unsigned *) esp_;
      check_uaddr_validity((uint8_t *) buffer, length);
      sema_down(&syscall_lock);
      if(file == 0)
      {
        f->eax = (int)input_getc();
      }
      else if(file < 0 || file == 1 || file > 127)
        f->eax = -1;
      else
      {
        if(!thread_current()->fd[file])
          f->eax = -1;
        else
          f->eax = (int)file_read(thread_current()->fd[file], buffer, length);
      }
      sema_up(&syscall_lock);


  }
  else if (syscall_num ==  SYS_WRITE){
      check_uaddr_validity(esp_, 4);
      int file = *(int *) esp_;
      esp_+=4;
      
      check_uaddr_validity(esp_, 4);
      void* buffer = (void *)*(uint32_t *)esp_;
      esp_+=4;
      
      check_uaddr_validity(esp_, 4);
      unsigned length = *(unsigned *) esp_;
      check_uaddr_validity((uint8_t *) buffer, length);
      sema_down(&syscall_lock);

      if(file < 1 || file > 127)
        f->eax = -1;
      else if(file == 1)
      {
        putbuf(buffer, length);
        f->eax = length;
      }
      else
      {
        if(!thread_current()->fd[file])
          f->eax = -1;
        else
          f->eax = (int)file_write(thread_current()->fd[file], buffer, length);
      }
      sema_up(&syscall_lock);
  }
  else if (syscall_num ==  SYS_SEEK){
      check_uaddr_validity(esp_, 4);
      int file = *(int *) esp_;
      esp_+=4;
      check_uaddr_validity(esp_, 4);
      unsigned position = *(unsigned *) esp_;
      sema_down(&syscall_lock);

      if(file > -1 && file < 128)
      {
        if(thread_current()->fd[file])
        {
          file_seek(thread_current()->fd[file], position);
        }
      }
      sema_up(&syscall_lock);
  }
  else if (syscall_num ==  SYS_TELL){
      check_uaddr_validity(esp_, 4);
      int file = *(int *) esp_;

      sema_down(&syscall_lock);

      if(file > -1 && file < 128)
      {
        if(thread_current()->fd[file])
        {
          f->eax = (unsigned)file_tell(thread_current()->fd[file]);
        }
        else
          f->eax = -1;
      }
      else
        f->eax = -1;
      sema_up(&syscall_lock);
  }
  else if (syscall_num ==  SYS_CLOSE){
      check_uaddr_validity(esp_, 4);
      sema_down(&syscall_lock);
      int file = *(int *) esp_;
      if(file > -1 && file < 128)
      {
        if(thread_current()->fd[file])
        {
          file_close(thread_current()->fd[file]);
          thread_current()->fd[file] = NULL;
        }
      }
      sema_up(&syscall_lock);

  }
  #ifdef VM

  else if (syscall_num ==  SYS_MMAP)
  {
    /* 0. check user address validity */ 
    check_uaddr_validity(esp_, 4);
    int file = *(int *) esp_;
    esp_+=4;
    check_uaddr_validity(esp_, 4);
    void *uaddr = *(uint32_t *)esp_;
    if(!uaddr || pg_ofs(uaddr))  //If uaddr is NULL or uaddr is not page-aligned
    {
      goto MMAP_FAILURE;
    }
    if(file <= 1 || file > 127)  // Fd 0, 1 are not mappable
    {
      goto MMAP_FAILURE;
    }

    sema_down(&syscall_lock); // Dealing with filesys func needs synchronization

    /* 1. Reopen the existing file in order to avoid any interference */
    struct file *reopened_file;
    off_t reopened_file_length;
    if(thread_current()->fd[file] == NULL)
    {
      sema_up(&syscall_lock);
      goto MMAP_FAILURE;
    }
    reopened_file = file_reopen(thread_current()->fd[file]);
    reopened_file_length = file_length(reopened_file);
    if(reopened_file == NULL || reopened_file_length == 0)
    {
      sema_up(&syscall_lock);
      goto MMAP_FAILURE;
    }

    /* 2. Mapping memory pages */
    int i = 0;
    void *vaddr = uaddr;
    uint32_t read_bytes;
    uint32_t zero_bytes;
    bool writable;

    if(thread_current()->fd[file]->deny_write)
      writable = false;
    else
      writable = true;
    // check whether the range of pages mapped overlaps any existing set of mapped pages
    for(; i < reopened_file_length ; i += PGSIZE)
    {
      if(find_ste(thread_current()->ST, vaddr) != NULL)  //Already occupied memory region
      {
        sema_up(&syscall_lock);
        goto MMAP_FAILURE;
      }
      vaddr += PGSIZE;
    }
    vaddr = uaddr;
    // map
    for(i = 0; i < reopened_file_length ; i += PGSIZE)
    {
      read_bytes = i + PGSIZE < reopened_file_length ? PGSIZE : reopened_file_length - i;
      zero_bytes = PGSIZE - read_bytes;
      if(!insert_STE_for_file(reopened_file, thread_current()->ST, vaddr, i, 
        read_bytes, zero_bytes, writable))
      {
        sema_up(&syscall_lock);
        goto MMAP_FAILURE;
      }
      vaddr += PGSIZE;
    }

    /* 3. Assign mapid */
    mapid_t mapid = 1;
    struct list_elem *it;
    struct mfile *new_mfile;
    if(!list_empty(&thread_current()->mfiles))
    {
      for(it = list_begin(&thread_current()->mfiles); 
        it != list_end(&thread_current()->mfiles); it = list_next(it))
      {
        if(list_entry(it, struct mfile, list_elem)->mapid > mapid)
          break;
        mapid++;
      }
    }
    new_mfile = (struct mfile *)malloc(sizeof(struct mfile));
    new_mfile->mapid = mapid;
    new_mfile->file = reopened_file;
    new_mfile->addr = uaddr;
    list_insert_ordered(&thread_current()->mfiles, &new_mfile->list_elem, 
      mapid_less, NULL);
    /* Final : release the lock and return the mapid */
    sema_up(&syscall_lock);
    f->eax = mapid;
    return;

  MMAP_FAILURE:
    f->eax = -1;
  }
  else if (syscall_num ==  SYS_MUNMAP)
  {
    /* 0. check user address validity */ 
    check_uaddr_validity(esp_, 4);
    mapid_t mapping = *(mapid_t *) esp_;
    struct list_elem *it;
    struct mfile *mfile;
    /* 1. Find corrosponding mapping */
    for(it = list_begin(&thread_current()->mfiles); 
        it != list_end(&thread_current()->mfiles); it = list_next(it))
    {
      if(list_entry(it, struct mfile, list_elem)->mapid == mapping)
      {
        mfile = list_entry(it, struct mfile, list_elem);
        break;
      }

    }
    if(it == list_end(&thread_current()->mfiles))   // There are no corrosponding mapping
      return;

    /* 2. unmap */
    int i = 0;
    off_t file_size = file_length(mfile->file);
    void *vaddr = mfile->addr;
    uint32_t file_bytes;
    sema_down(&syscall_lock); // Dealing with filesys func needs synchronization
    for(; i < file_size ; i += PGSIZE)
    {
      file_bytes = i + PGSIZE < file_size ? PGSIZE : file_size - i;
      if(!unmap_STE(thread_current()->ST, thread_current()->pagedir, vaddr, mfile->file,
        file_bytes, i))
      {
        sema_up(&syscall_lock);
        return;
      }
      vaddr += PGSIZE;
    }

    /* Final : Remove the mapping from the mapped file list, and free it */
    list_remove(&mfile->list_elem);
    file_close(mfile->file);
    free(mfile);
    sema_up(&syscall_lock);
  }

  #endif

  else if (syscall_num ==  SYS_CHDIR)
  {
    check_uaddr_validity(esp_, 4);
    char* dir = (char *)*(uint32_t *)esp_;
    sema_down(&syscall_lock);
    f->eax = filesys_chdir(dir);
    sema_up(&syscall_lock);
  }

  else if (syscall_num ==  SYS_MKDIR)
  {
    check_uaddr_validity(esp_, 4);
    char* dir = (char *)*(uint32_t *)esp_;
    sema_down(&syscall_lock);
    f->eax = filesys_create(dir, 0, true);
    sema_up(&syscall_lock);
  }

  else if (syscall_num ==  SYS_READDIR)
  {
    check_uaddr_validity(esp_, 4);
    int file_d = *(int *) esp_;
    esp_+=4;
    check_uaddr_validity(esp_, 4);
    char *name = (char *)*(uint32_t *)esp_;
    struct inode *inode;
    struct file *file;
    sema_down(&syscall_lock);

    if((file = thread_current()->fd[file_d]) == NULL)
    {
      sema_up(&syscall_lock);
      return;
    }

    inode = file_get_inode(file);

    if(!inode || !inode_is_dir(inode))
    {
      f->eax = -1;
      sema_up(&syscall_lock);
      return;
    }
    //file->directory
    f->eax = dir_readdir(file->directory ,name);
    sema_up(&syscall_lock);
  }

  else if (syscall_num ==  SYS_ISDIR)
  {
    check_uaddr_validity(esp_, 4);
    int file_d = *(int *) esp_;
    esp_+=4;
    struct file *file;
    sema_down(&syscall_lock);
    if((file = thread_current()->fd[file_d]) == NULL)
      f->eax = -1;
    f->eax = inode_is_dir(file_get_inode(file));
    sema_up(&syscall_lock);
  }

  else if (syscall_num ==  SYS_INUMBER)
  {
    check_uaddr_validity(esp_, 4);
    int file_d = *(int *) esp_;
    esp_+=4;
    struct file *file;
    sema_down(&syscall_lock);
    if((file = thread_current()->fd[file_d]) == NULL)
      f->eax = -1;
    f->eax = inode_get_inumber(file_get_inode(file));
    sema_up(&syscall_lock); 
  }

  else
    f->eax = -1;
}



