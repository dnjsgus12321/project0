#ifndef FILESYS_CACHE_H
#define FILESYS_CACHE_H

#include "devices/block.h"

/* structure for cache entry */
struct CE
{
	block_sector_t disk_sector;          /* Location of disk where this was came from */
	uint8_t buffer[BLOCK_SECTOR_SIZE];   /* The actual content */

	bool is_dirty;     /* To decide whether we use write back or not */ 
	bool is_accessed;  /* For Clock algorithm */

	bool valid;  /* Whether this entry is currently used or not */
};

/* The size of buffer cache */
#define BUFFER_CACHE_SIZE 64

void cache_init(void);    /* We do this when filesys_init */
void cache_finish(void); /* We do this when filesys_close */

/* Read from buffer cache */
void cache_read(block_sector_t sector, void* dst, int sector_ofs, int chunk_size);

/* Write to buffer cache */
void cache_write(block_sector_t sector, void* src, int sector_ofs, int chunk_size); 

#endif