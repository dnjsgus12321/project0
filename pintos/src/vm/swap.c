#include <bitmap.h>
#include "devices/block.h"
#include "vm/swap.h"
#include "threads/synch.h"
#include "threads/malloc.h"
#include "threads/thread.h"
#include "threads/vaddr.h"

struct block* swap_disk;
struct bitmap* swap_map;  /* 1: swap_index filled, 0:swap_index_empty */
static struct lock swap_lock;

static const uint32_t SECTORSPERPAGE = PGSIZE / BLOCK_SECTOR_SIZE; 
static uint32_t remain_swap_page;  /* Number of remaining pages in the swap_disk */

void swap_init(void)
{
  swap_disk = block_get_role(BLOCK_SWAP);
  if(!swap_disk)
    thread_exit();
  remain_swap_page = block_size(swap_disk) / SECTORSPERPAGE;
  swap_map = bitmap_create(remain_swap_page);
  lock_init(&swap_lock);
}

uint32_t swap_out(void* kpage)
{
  uint32_t possible_swap_index = bitmap_scan(swap_map, 0, 1, false);
  uint32_t i = 0;

  for(; i < SECTORSPERPAGE; i++)
  {
    block_write(swap_disk, possible_swap_index * SECTORSPERPAGE + i, 
                     kpage + BLOCK_SECTOR_SIZE*i);
  }
  bitmap_set(swap_map, possible_swap_index, true);
  return possible_swap_index;
}

void swap_in(void* kpage, uint32_t swap_index)
{
  uint32_t i = 0;
  if(!bitmap_test(swap_map, swap_index))
    thread_exit();
  for(; i < SECTORSPERPAGE; i++)
  {
    block_read(swap_disk, swap_index * SECTORSPERPAGE + i,
                      kpage + BLOCK_SECTOR_SIZE*i);
  }
  bitmap_set(swap_map, swap_index, false);
}

void swap_free(uint32_t swap_index)
{
  if(!bitmap_test(swap_map, swap_index))
    thread_exit();
  bitmap_set(swap_map, swap_index, false);
}

  
