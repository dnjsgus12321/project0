#include <string.h>
#include "threads/synch.h"
#include "filesys/cache.h"
#include "filesys/filesys.h"
#include "devices/block.h"

/* The buffer cache */
static struct CE buffer_cache[BUFFER_CACHE_SIZE];

/* Lock for the buffer cache */
static struct lock cache_lock;

/* The clock hand for eviction algorithm */
static int clock_hand;



/* The static functions inside this source file */
static void write_back_to_disk(struct CE*);
static struct CE* cache_lookup(block_sector_t sector);
static struct CE* find_free_CE(void);
static struct CE* evict_cache_entry(void);

/* Initialize all the cache entries by following procedures.
   1. Mark all entry's 'valid' as false
   2. Clear all buffer in each entries
   3. Initialize clock_hand as 0  */
void cache_init(void)
{
	lock_init(&cache_lock);

	int i = 0;
	for(; i < BUFFER_CACHE_SIZE; i++)
	{
		buffer_cache[i].valid = false;
		memset(buffer_cache[i].buffer, 0, BLOCK_SECTOR_SIZE);
	}
	clock_hand = 0;
}

/* We do this when filesys is closed.
   1. Write all contents of cache back to the disk */
void cache_finish(void)
{
	//lock_acquire(&cache_lock);

	int i = 0;
	for(; i < BUFFER_CACHE_SIZE; i++)
	{
		/* If the cache_entry is currently not used, just continue */
		if(buffer_cache[i].valid == false)
			continue;

		/* If it is currently used, write back to disk */
		write_back_to_disk(&buffer_cache[i]);
		buffer_cache[i].valid = false;
	}
	//lock_release(&cache_lock);
}

/* Write the given cache_entry back to the disk if it is dirty.
   Lock must be already acquired by the caller */
static void write_back_to_disk(struct CE* cache_entry)
{
	if(cache_entry->is_dirty)
		block_write(fs_device, cache_entry->disk_sector, cache_entry->buffer);
}

/* 1. Find the cache entry corrosponding to given sector number
   2. If cache hit, return it
   3. If cache miss, return NULL
*/	
static struct CE* cache_lookup(block_sector_t sector)
{
	int i = 0;
	struct CE* found_cache_entry = NULL;
	for(; i < BUFFER_CACHE_SIZE; i++)
	{
		/* If the cache_entry is currently not used, just continue */
		if(buffer_cache[i].valid == false)
			continue;

		if(buffer_cache[i].disk_sector == sector)
		{
			found_cache_entry = &buffer_cache[i];
			break;
		}
	}

	return found_cache_entry;
}

/* 1. Find the cache entry corrosponding to given sector number
   2. If cache miss, update the cache by read the content from the disk
      2-1. If cache is already full, evict some cache entry
   3. memcpy the required content to the dst.
*/
void cache_read(block_sector_t sector, void* dst, int sector_ofs, int chunk_size)
{
	lock_acquire(&cache_lock);
	/*1. Find the cache entry corrosponding to given sector number*/
	struct CE* found_cache_entry = cache_lookup(sector);

	/*2. If cache miss, update the cache*/
	if(!found_cache_entry)
	{
		found_cache_entry = find_free_CE();
		found_cache_entry->valid = true;
		found_cache_entry->disk_sector = sector;
		found_cache_entry->is_dirty = false;
		block_read(fs_device, sector, found_cache_entry->buffer);
	}
	found_cache_entry->is_accessed = true;

	/*3. memcpy the required content to the dst.*/
	memcpy(dst, found_cache_entry->buffer + sector_ofs, chunk_size);

	lock_release(&cache_lock);
}

/* 1. Find the cache entry corrosponding to given sector number
   2. If cache miss, update the cache by read the content from the disk
      2-1. If cache is already full, evict some cache entry
   3. memcpy the written content to the cache entry's buffer.
*/
void cache_write(block_sector_t sector, void* src, int sector_ofs, int chunk_size)
{
	lock_acquire(&cache_lock);
	/*1. Find the cache entry corrosponding to given sector number*/
	struct CE* found_cache_entry = cache_lookup(sector);

	/*2. If cache miss, update the cache*/
	if(!found_cache_entry)
	{
		found_cache_entry = find_free_CE();
		found_cache_entry->valid = true;
		found_cache_entry->disk_sector = sector;
		block_read(fs_device, sector, found_cache_entry->buffer);
	}
	found_cache_entry->is_accessed = true;
	found_cache_entry->is_dirty = true;

	/*3. memcpy the written content to the cache entry's buffer.*/
	memcpy(found_cache_entry->buffer + sector_ofs, src, chunk_size);

	lock_release(&cache_lock);
}

/* 1. Find the cache entry which is not currently used and return it
   2. If there are no such entries, evict some entry back to the disk
   Lock is already acquired by the caller, so we should not acquire it again
*/
static struct CE* find_free_CE(void)
{
	int i = 0;
	/*1. Find the cache entry which is not currently used and return it */
	for(; i < BUFFER_CACHE_SIZE; i++)
	{
		if(buffer_cache[i].valid == false)
		{
			return &buffer_cache[i];
		}
	}

	/* 2. If there are no such entries, evict some entry back to the disk */
	return evict_cache_entry();
} 

/* 1. Evict cache entry using clock algorithm and return it 
   Lock is already adquired by the caller, so we should not acquire it again
*/
static struct CE* evict_cache_entry(void)
{
	/* Find a cache entry to evict */
	while(1)
	{
		if(buffer_cache[clock_hand].is_accessed)
		{
			buffer_cache[clock_hand].is_accessed = false;
			clock_hand++;
			if(clock_hand >= BUFFER_CACHE_SIZE)
				clock_hand = 0;
			continue;
		}
		else
			break;
	}

	/* Evict the cache entry */
	write_back_to_disk(&buffer_cache[clock_hand]);
	buffer_cache[clock_hand].valid = false;
	return &buffer_cache[clock_hand];
}