#include "vm/frame.h"
#include <stdio.h>
#include <string.h>
#include "threads/synch.h"
#include "threads/palloc.h"
#include "threads/malloc.h"
#include "threads/thread.h"
#include "threads/vaddr.h"
#include "userprog/pagedir.h"
#include "userprog/syscall.h"
#include "vm/swap.h"
#include "vm/page.h"
#include "filesys/file.h"
#include "threads/vaddr.h"

static struct list* frame_table_list;
static struct lock frame_lock;
static struct list_elem* clock_hand;


void frame_init(void)
{

  frame_table_list = (struct list*)malloc(sizeof(struct list));
  list_init(frame_table_list);
  clock_hand = NULL;

  lock_init(&frame_lock);
}

void*
allocate_frame(struct thread* cur, void *uaddr, bool PAL_ZERO_)
{
  struct FTE *fte = (struct FTE*)malloc(sizeof(struct FTE));
  struct FTE* victim_fte;
  uint32_t swap_index;
  fte->uaddr = uaddr;
  fte->fthread = cur;
  uint8_t *kpage;
  //lock_acquire(&frame_lock);

  if (PAL_ZERO_)
    kpage = palloc_get_page(PAL_USER | PAL_ZERO);
  else
    kpage = palloc_get_page(PAL_USER);

  if (kpage==NULL)
  {
    do
    {
      victim_fte = victim_frame();
      swap_index = swap_out(victim_fte->paddr);
    }while(!ST_update_swap_out(victim_fte->fthread->ST, victim_fte->uaddr, swap_index));
    pagedir_clear_page(victim_fte->fthread->pagedir, victim_fte->uaddr);
    free_frame(victim_fte->paddr);
    /*victim_fte = victim_frame();
    swap_index = swap_out(victim_fte->paddr);
    ST_update_swap_out(victim_fte->fthread->ST, victim_fte->uaddr, swap_index);
    pagedir_clear_page(victim_fte->fthread->pagedir, victim_fte->uaddr);
    free_frame(victim_fte->paddr);*/
    if(PAL_ZERO_)
      kpage = palloc_get_page(PAL_USER | PAL_ZERO);
    else
      kpage = palloc_get_page(PAL_USER);
  }
  fte->paddr = kpage;
  fte->pin = true;
  list_push_back(frame_table_list, &fte->list_elem);
  //lock_release(&frame_lock);
  return kpage;
}


void
free_frame(void *kpage)
{
  struct list_elem* elem;
  struct FTE* fte;
  for(elem = list_begin(frame_table_list); elem != list_end(frame_table_list); 
    elem = list_next(elem))
  {
    fte = list_entry(elem, struct FTE, list_elem);
    if(fte->paddr == kpage)
    {
      palloc_free_page(fte->paddr);
      if(&fte->list_elem == clock_hand)
        clock_hand = list_next(clock_hand);
      list_remove(&fte->list_elem);
      free(fte);
      return;
    }
  }
  thread_exit();
}

void
remove_frame(void *kpage)
{
  struct list_elem* elem;
  struct FTE* fte;
  for(elem = list_begin(frame_table_list); elem != list_end(frame_table_list); 
    elem = list_next(elem))
  {
    fte = list_entry(elem, struct FTE, list_elem);
    if(fte->paddr == kpage)
    {
      lock_acquire(&frame_lock);
      if(&fte->list_elem == clock_hand)
        clock_hand = list_next(clock_hand);
      list_remove(&fte->list_elem);
      free(fte);
      lock_release(&frame_lock);
      return;
    }
  }
  thread_exit();
}

struct FTE* 
victim_frame(void)
{
  struct FTE* fte;
  void* uaddr;
  void* kaddr;
  static int cnt = 0;
  thread_current()->finding_victim = true;
  
  if (list_empty(frame_table_list))
    PANIC("Frame table is empty");


  if (clock_hand == NULL || clock_hand == list_end(frame_table_list)){
    clock_hand = list_begin(frame_table_list);
  }

 
  while(true){

    //printf("%x\n", clock_hand);
    fte = list_entry(clock_hand, struct FTE, list_elem);
    if(fte->pin)
      goto next;

    uaddr = fte->uaddr;
    kaddr = pagedir_get_page(fte->fthread->pagedir, uaddr);


    //fte->fthread
    //thread_current()

    if (pagedir_is_accessed(fte->fthread->pagedir, uaddr) || 
      pagedir_is_accessed(fte->fthread->pagedir, kaddr))
    {
      pagedir_set_accessed(fte->fthread->pagedir, uaddr, 0);
      pagedir_set_accessed(fte->fthread->pagedir, kaddr, 0);
    }
    else{
      clock_hand = list_next(clock_hand);
      break;
    }

  next:  
    clock_hand = list_next(clock_hand);
    if (clock_hand == list_end(frame_table_list)){
      clock_hand = list_begin(frame_table_list);
    }
    
  }
  thread_current()->finding_victim = false;
  cnt++;

  return fte;
}

bool
load_page_for_file(struct STE* ste)
{
  //assert(ste->page_state == FROM_FILE);
  lock_acquire(&frame_lock);
  uint8_t *kpage = allocate_frame(thread_current(), ste->uaddr, false);
  if (kpage == NULL){
    lock_release(&frame_lock);
    return false;
  }
  lock_acquire(&ste->file->filesys_lock);

  if (file_read_at(ste->file, kpage, ste->read_bytes, ste->offset) != 
    (int) ste->read_bytes)
  {
    free_frame(kpage);
    lock_release(&ste->file->filesys_lock);
    lock_release(&frame_lock);
    return false;
  }
  lock_release(&ste->file->filesys_lock);
  memset(kpage+ste->read_bytes, 0, ste->zero_bytes);
  
  if (!pagedir_set_page(thread_current()->pagedir ,ste->uaddr, kpage, ste->writable))
  {
    free_frame(kpage);
    lock_release(&frame_lock);
    return false;
  }
  ste->paddr = kpage;
  ste->present = true;
  ste->page_state = IN_RAM;
  pin_or_unpin(kpage, false);
  lock_release(&frame_lock);
  pagedir_set_dirty(thread_current()->pagedir, kpage, 0);
  pagedir_set_accessed(thread_current()->pagedir, kpage, 0);
  return true;
}

bool
load_page_for_setup_stack(void)
{
  uint8_t *kpage;
  lock_acquire(&frame_lock);
  kpage = allocate_frame(thread_current(), ((uint8_t*) PHYS_BASE) - PGSIZE, true);
  //printf("%x, %x\n", pg_round_down(PHYS_BASE - PGSIZE), PHYS_BASE - PGSIZE)
  if(kpage==NULL){
    lock_release(&frame_lock);
    return false;
  }
  if(!pagedir_set_page(thread_current()->pagedir, ((uint8_t*) PHYS_BASE) - PGSIZE, kpage, true)){
    free_frame(kpage);
    lock_release(&frame_lock);
    return false;
  }
  insert_STE_for_stack(thread_current()->ST, ((uint8_t*)PHYS_BASE) - PGSIZE, kpage);
  pin_or_unpin(kpage, false);
  lock_release(&frame_lock);
  thread_current()->end_of_stack = PHYS_BASE - PGSIZE;
  pagedir_set_dirty(thread_current()->pagedir, kpage, 0);
  pagedir_set_accessed(thread_current()->pagedir, kpage, 0);
  return true;
}

bool
load_page_for_stack_growth(void* fault_page)
{  
  lock_acquire(&frame_lock);
  void* kpage = allocate_frame(thread_current(), fault_page, true);
  if(kpage==NULL){
    lock_release(&frame_lock);
    return false;
  }
  if(!pagedir_set_page(thread_current()->pagedir, fault_page, kpage, true))
  {
    free_frame(kpage);
    lock_release(&frame_lock);
    return false;
  }
  insert_STE_for_stack(thread_current()->ST, fault_page, kpage);
  pin_or_unpin(kpage, false);
  lock_release(&frame_lock);
  thread_current()->end_of_stack -= PGSIZE;
  pagedir_set_dirty(thread_current()->pagedir, kpage, 0);
  pagedir_set_accessed(thread_current()->pagedir, kpage, 0);
  return true;
}

bool
load_page_from_swap_disk(void* fault_page, struct STE* ste)
{
  lock_acquire(&frame_lock);
  void* kpage = allocate_frame(thread_current(), fault_page, true);
  if(kpage==NULL){
    lock_release(&frame_lock);
    return false;
  }
  if(!pagedir_set_page(thread_current()->pagedir, fault_page, kpage, true))
  {
    free_frame(kpage);
    lock_release(&frame_lock);
    return false;
  }
  swap_in(kpage, ste->swap_index);
  ste->page_state = IN_RAM;
  ste->present = true;
  ste->paddr = kpage;
  pin_or_unpin(kpage, false);
  lock_release(&frame_lock);
  pagedir_set_dirty(thread_current()->pagedir, kpage, 0);
  pagedir_set_accessed(thread_current()->pagedir, kpage, 0);
  return true;
}

void
pin_or_unpin(void* kpage, bool pin)
{
  struct list_elem* elem;
  struct FTE* fte;
  for(elem = list_begin(frame_table_list); elem != list_end(frame_table_list); 
    elem = list_next(elem))
  {
    fte = list_entry(elem, struct FTE, list_elem);
    if(fte->paddr == kpage)
    {
      fte->pin = pin;
      return;
    }
  }
  thread_exit();
}

