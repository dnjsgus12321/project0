#ifndef FILESYS_FILESYS_H
#define FILESYS_FILESYS_H

#include <stdbool.h>
#include "filesys/off_t.h"

/* Sectors of system file inodes. */
#define FREE_MAP_SECTOR 0       /* Free map file inode sector. */
#define ROOT_DIR_SECTOR 1       /* Root directory file inode sector. */

/* Maximum length for file and path */
#define MAX_FILE_NAME 14
#define MAX_PATH_NAME 256

/* Block device that contains the file system. */
struct block *fs_device;

void filesys_init (bool format);
void filesys_done (void);
bool filesys_create (const char *path, off_t initial_size, bool is_dir);
struct file *filesys_open (const char *path);
bool filesys_remove (const char *path);
bool filesys_mkdir (char *path);
bool filesys_chdir(char *dir);
//bool filesys_readdir(int file_d, char* name);

#endif /* filesys/filesys.h */
