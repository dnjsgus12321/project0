#include "userprog/process.h"
#include <debug.h>
#include <inttypes.h>
#include <round.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <hash.h>
#include "userprog/gdt.h"
#include "userprog/pagedir.h"
#include "userprog/tss.h"
#include "filesys/directory.h"
#include "filesys/file.h"
#include "filesys/filesys.h"
#include "threads/flags.h"
#include "threads/init.h"
#include "threads/interrupt.h"
#include "threads/palloc.h"
#include "threads/thread.h"
#include "threads/vaddr.h"
#include "lib/user/syscall.h"
#include "list.h"
#include "vm/frame.h"
#include "vm/page.h"

static thread_func start_process NO_RETURN;
static bool load (const char *cmdline, void (**eip) (void), void **esp);
static void push_arg_to_stack (char *first_command, char *args, void **esp);
static bool put_user (uint8_t *udst, uint8_t byte);
void push2list_per_byte (int i, uint32_t argv_addr, uint8_t addbyte_list[]);
tid_t process_semi_wait (tid_t child_tid UNUSED); 
//static int get_user (const uint8_t *uaddr);
/* Starts a new thread running a user program loaded from
   FILENAME.  The new thread may be scheduled (and may even exit)
   before process_execute() returns.  Returns the new process's
   thread id, or TID_ERROR if the thread cannot be created. */
tid_t
process_execute (const char *file_name) 
{
  char *fn_copy;
  tid_t tid;

  /* Make a copy of FILE_NAME.
     Otherwise there's a race between the caller and load(). */
  fn_copy = palloc_get_page (0);
  if (fn_copy == NULL)
    return TID_ERROR;
  strlcpy (fn_copy, file_name, PGSIZE);

  /* Create a new thread to execute FILE_NAME. */
  tid = thread_create (file_name, PRI_DEFAULT, start_process, fn_copy);
  if (tid == TID_ERROR)
    palloc_free_page (fn_copy); 
  else
    tid = process_semi_wait(tid);

  return tid;
}
/* A thread function that loads a user process and starts it
   running. */


static bool
put_user (uint8_t *udst, uint8_t byte)
{
  int error_code;
  asm ("movl $1f, %0; movb %b2, %1; 1:"
       : "=&a" (error_code), "=m" (*udst) : "q" (byte));
  return error_code != -1;
}

static void
start_process (void *file_name_)
{
  char *file_name = file_name_;
  char * args;
  struct intr_frame if_;
  bool success;
  char *first_command;
  struct thread *cur = thread_current ();
  //printf("I'm starting~~\n");
  first_command = strtok_r(file_name_, " ", &args);
  strlcpy (thread_current()->name, first_command, sizeof thread_current()->name);
  /* Initialize interrupt frame and load executable. */
  memset (&if_, 0, sizeof if_);
  if_.gs = if_.fs = if_.es = if_.ds = if_.ss = SEL_UDSEG;
  if_.cs = SEL_UCSEG;
  if_.eflags = FLAG_IF | FLAG_MBS;
  success = load (first_command, &if_.eip, &if_.esp);
  //printf("%d: load success: %d\n", cur->tid, success);
  if (success)
      push_arg_to_stack(first_command, args, &if_.esp);


  
  /* If load failed, quit. */
  palloc_free_page (file_name);

  if(!thread_current()->working_directory)
    thread_current()->working_directory = dir_open_root();

  if (!success) {
    thread_current()->load_succeed = false;
    //thread_current()->exit_status = -1;
  }
  //printf("%d: start sema in start\n", thread_current()->tid);
  sema_up(&cur->load_initial_lock);
  //printf("%d: betw sema in start\n", thread_current()->tid);
  sema_down(&cur->load_final_lock);
  //printf("%d: end sema in start\n", thread_current()->tid);

  if (!success) {
    thread_exit ();
  }
  /* Start the user process by simulating a return from an
     interrupt, implemented by intr_exit (in
     threads/intr-stubs.S).  Because intr_exit takes all of its
     arguments on the stack in the form of a `struct intr_frame',
     we just point the stack pointer (%esp) to our stack frame
     and jump to it. */
  asm volatile ("movl %0, %%esp; jmp intr_exit" : : "g" (&if_) : "memory");
  NOT_REACHED ();
}

void
push2list_per_byte (int i, uint32_t argv_addr, uint8_t addbyte_list[])
{ 
  uint8_t byte_mask = 0xff;
  for (int ii=0; ii<4; ii++){
      addbyte_list[(i)*4+ii] = ((argv_addr>>(24-8*ii)) & byte_mask);
  }
  return;
}

void
push_arg_to_stack (char *first_command, char *args, void **esp)
{
  char *next_ptr;
  char *arg;
  int argv_cnt = 0;
  int added_size;
  int total_added_size = 0;
  char *argv[50];
  argv[argv_cnt] = first_command;
  argv_cnt +=1;
  arg = strtok_r(args, " ", &next_ptr);
  
  while(arg){
      argv[argv_cnt] = arg;
      argv_cnt +=1;
      arg = strtok_r(NULL, " ", &next_ptr);
  }
  int addbyte_list_len = (argv_cnt+1)*4;
  uint8_t addbyte_list[addbyte_list_len];
  for (int i=0; i<argv_cnt; i++){ 
      arg = argv[argv_cnt-i-1];
      *esp-= (strlen(arg)+1);
      added_size = strlcpy(*esp, arg, strlen(arg)+1) + 1;
      uint32_t argv_addr = (uint32_t) *esp;
      if (i==0){ 
          push2list_per_byte (i, 0, addbyte_list);
      }
      push2list_per_byte (i+1, argv_addr, addbyte_list);
      total_added_size += added_size;
  }
  *esp-=1;
  if (total_added_size % 4){
      
      int word_align_size = 4-total_added_size%4;
      
      for (int i=0; i<word_align_size; i++){
          if(!put_user(*esp, 0)){
              printf("put_user error\n");
              thread_exit();
          }
          *esp-=1;
      }
  }
  
  for(int i=0; i<addbyte_list_len; i++){
      if(!put_user(*esp, addbyte_list[i])){
          printf("put_user in addbyre error\n");
          thread_exit();
      }
      *esp-=1;
  }

  uint32_t argv_addr = (uint32_t)*esp+1;
  uint8_t argvbyte_list[4];
  push2list_per_byte (0, argv_addr, argvbyte_list);
  for(int i=0; i<4; i++){
      if(!put_user(*esp, argvbyte_list[i])){
          printf("put argv error\n");
          //exit(-1);
          thread_exit();
      }
      *esp-=1;
  }
  uint8_t argcbyte_list[4];
  push2list_per_byte (0, argv_cnt, argcbyte_list);
  
  for(int i=0; i<4; i++){
      if(!put_user(*esp, argcbyte_list[i])){
          printf("put argc error\n");
          //exit(-1);
          thread_exit();
      }
      *esp-=1;
  }
  
  for(int i=0; i<4; i++){
      if(!put_user(*esp, 0)){
          printf("put return address error\n");
          //exit(-1);
          thread_exit();
      }
      *esp-=1;
  }
  *esp+=1;

  //hex_dump((uintptr_t)*esp , *esp, (uint32_t) PHYS_BASE - (uint32_t) *esp, true);
}

/* Waits for thread TID to die and returns its exit status.  If
   it was terminated by the kernel (i.e. killed due to an
   exception), returns -1.  If TID is invalid or if it was not a
   child of the calling process, or if process_wait() has already
   been successfully called for the given TID, returns -1
   immediately, without waiting.

   This function will be implemented in problem 2-2.  For now, it
   does nothing. */

tid_t
process_semi_wait (tid_t child_tid UNUSED) 
{
  struct list_elem *cursor;
  struct thread *t;
  int tid = child_tid;

  cursor = list_begin(&thread_current()->child_list);

  while(cursor != list_end(&thread_current()->child_list))
  {
    t = list_entry(cursor, struct thread, child_elem);
    if(t->tid == child_tid)
    {
      
      //printf("%d: start sema in semi wait\n", thread_current()->tid);
      sema_down(&t->load_initial_lock);
      if (!t->load_succeed){
          tid = TID_ERROR;
          list_remove(&t->child_elem);
      }
      //printf("%d: betw sema in semi wait\n", thread_current()->tid);
      sema_up(&t->load_final_lock);
      //printf("%d: end sema in semi wait\n", thread_current()->tid);
      break;
    }
    cursor = list_next(cursor);
  }
  return tid;
}

int
process_wait (tid_t child_tid UNUSED) 
{
  struct list_elem *cursor;
  struct thread *t;
  int status = -1;
  cursor = list_begin(&thread_current()->child_list);

  while(cursor != list_end(&thread_current()->child_list))
  {
    t = list_entry(cursor, struct thread, child_elem);
    if(t->tid == child_tid)
    {
      //printf("%d: bef sema_down in wait\n", thread_current()->tid);
      sema_down(&t->initial_lock);
      list_remove(&t->child_elem);
      status = t->exit_status;

      sema_up(&t->final_lock);
      break;
    }
    cursor = list_next(cursor);
  }
  return status;
}

/* Free the current process's resources. */
void
process_exit (void)
{
  struct thread *cur = thread_current ();
  uint32_t *pd;
  int i;

  if(cur->working_directory)
    dir_close(cur->working_directory);

  /* Destroy the current process's page directory and switch back
     to the kernel-only page directory. */
  pd = cur->pagedir;

  #ifdef VM
  struct list_elem *it;
  struct mfile *mfile;
  off_t file_size;
  void *vaddr;
  uint32_t file_bytes;
  while(!list_empty(&thread_current()->mfiles))
  {
    it = list_begin(&thread_current()->mfiles);
    mfile = list_entry(it, struct mfile, list_elem);
    vaddr = mfile->addr;
    lock_acquire(&mfile->file->filesys_lock);
    file_size = file_length(mfile->file);
    for(i = 0; i < file_size ; i += PGSIZE)
    {
      file_bytes = i + PGSIZE < file_size ? PGSIZE : file_size - i;
      unmap_STE(cur->ST, pd, vaddr, mfile->file,
        file_bytes, i);
      vaddr += PGSIZE;
    }
    list_remove(&mfile->list_elem);
    //file_close(mfile->file);
    lock_release(&mfile->file->filesys_lock);
    free(mfile);
  }
  ST_free(cur->ST);
  #endif

  if (pd != NULL) 
    {
      /* Correct ordering here is crucial.  We must set
         cur->pagedir to NULL before switching page directories,
         so that a timer interrupt can't switch back to the
         process page directory.  We must activate the base page
         directory before destroying the process's page
         directory, or our active page directory will be one
         that's been freed (and cleared). */
      cur->pagedir = NULL;
      pagedir_activate (NULL);
      pagedir_destroy (pd);
    }
  for(i=0; i < 128; i++)
    file_close(cur->fd[i]);

  file_close(cur->loaded_file);
  sema_up(&cur->initial_lock);
  sema_down(&cur->final_lock);
  printf("%s: exit(%d)\n", thread_current()->name, thread_current()->exit_status);
}

/* Sets up the CPU for running user code in the current
   thread.
   This function is called on every context switch. */
void
process_activate (void)
{
  struct thread *t = thread_current ();

  /* Activate thread's page tables. */
  pagedir_activate (t->pagedir);

  /* Set thread's kernel stack for use in processing
     interrupts. */
  tss_update ();
}

/* We load ELF binaries.  The following definitions are taken
   from the ELF specification, [ELF1], more-or-less verbatim.  */

/* ELF types.  See [ELF1] 1-2. */
typedef uint32_t Elf32_Word, Elf32_Addr, Elf32_Off;
typedef uint16_t Elf32_Half;

/* For use with ELF types in printf(). */
#define PE32Wx PRIx32   /* Print Elf32_Word in hexadecimal. */
#define PE32Ax PRIx32   /* Print Elf32_Addr in hexadecimal. */
#define PE32Ox PRIx32   /* Print Elf32_Off in hexadecimal. */
#define PE32Hx PRIx16   /* Print Elf32_Half in hexadecimal. */

/* Executable header.  See [ELF1] 1-4 to 1-8.
   This appears at the very beginning of an ELF binary. */
struct Elf32_Ehdr
  {
    unsigned char e_ident[16];
    Elf32_Half    e_type;
    Elf32_Half    e_machine;
    Elf32_Word    e_version;
    Elf32_Addr    e_entry;
    Elf32_Off     e_phoff;
    Elf32_Off     e_shoff;
    Elf32_Word    e_flags;
    Elf32_Half    e_ehsize;
    Elf32_Half    e_phentsize;
    Elf32_Half    e_phnum;
    Elf32_Half    e_shentsize;
    Elf32_Half    e_shnum;
    Elf32_Half    e_shstrndx;
  };

/* Program header.  See [ELF1] 2-2 to 2-4.
   There are e_phnum of these, starting at file offset e_phoff
   (see [ELF1] 1-6). */
struct Elf32_Phdr
  {
    Elf32_Word p_type;
    Elf32_Off  p_offset;
    Elf32_Addr p_vaddr;
    Elf32_Addr p_paddr;
    Elf32_Word p_filesz;
    Elf32_Word p_memsz;
    Elf32_Word p_flags;
    Elf32_Word p_align;
  };

/* Values for p_type.  See [ELF1] 2-3. */
#define PT_NULL    0            /* Ignore. */
#define PT_LOAD    1            /* Loadable segment. */
#define PT_DYNAMIC 2            /* Dynamic linking info. */
#define PT_INTERP  3            /* Name of dynamic loader. */
#define PT_NOTE    4            /* Auxiliary info. */
#define PT_SHLIB   5            /* Reserved. */
#define PT_PHDR    6            /* Program header table. */
#define PT_STACK   0x6474e551   /* Stack segment. */

/* Flags for p_flags.  See [ELF3] 2-3 and 2-4. */
#define PF_X 1          /* Executable. */
#define PF_W 2          /* Writable. */
#define PF_R 4          /* Readable. */

static bool setup_stack (void **esp);
static bool validate_segment (const struct Elf32_Phdr *, struct file *);
static bool load_segment (struct file *file, off_t ofs, uint8_t *upage,
                          uint32_t read_bytes, uint32_t zero_bytes,
                          bool writable);

/* Loads an ELF executable from FILE_NAME into the current thread.
   Stores the executable's entry point into *EIP
   and its initial stack pointer into *ESP.
   Returns true if successful, false otherwise. */
bool
load (const char *file_name, void (**eip) (void), void **esp) 
{
  struct thread *t = thread_current ();
  struct Elf32_Ehdr ehdr;
  struct file *file = NULL;
  off_t file_ofs;
  bool success = false;
  int i;
  //printf("%d: loading~~~\n", thread_current()->tid);
  /* Allocate and activate page directory. */
  t->pagedir = pagedir_create ();
  if (t->pagedir == NULL) 
    goto done;
  t->ST = ST_create();
  process_activate ();

  /* Open executable file. */
  file = filesys_open (file_name);
  if (file == NULL) 
    {
      printf ("load: %s: open failed\n", file_name);
      goto done;
    }
  t->loaded_file = file;

  /* Read and verify executable header. */
  if (file_read (file, &ehdr, sizeof ehdr) != sizeof ehdr
      || memcmp (ehdr.e_ident, "\177ELF\1\1\1", 7)
      || ehdr.e_type != 2
      || ehdr.e_machine != 3
      || ehdr.e_version != 1
      || ehdr.e_phentsize != sizeof (struct Elf32_Phdr)
      || ehdr.e_phnum > 1024) 
    {
      printf ("load: %s: error loading executable\n", file_name);
      goto done; 
    }

  /* Read program headers. */
  file_ofs = ehdr.e_phoff;
  for (i = 0; i < ehdr.e_phnum; i++) 
    {
      struct Elf32_Phdr phdr;

      if (file_ofs < 0 || file_ofs > file_length (file))
        goto done;
      file_seek (file, file_ofs);

      if (file_read (file, &phdr, sizeof phdr) != sizeof phdr)
        goto done;
      file_ofs += sizeof phdr;
      switch (phdr.p_type) 
        {
        case PT_NULL:
        case PT_NOTE:
        case PT_PHDR:
        case PT_STACK:
        default:
          /* Ignore this segment. */
          break;
        case PT_DYNAMIC:
        case PT_INTERP:
        case PT_SHLIB:
          goto done;
        case PT_LOAD:
          if (validate_segment (&phdr, file)) 
            {
              bool writable = (phdr.p_flags & PF_W) != 0;
              uint32_t file_page = phdr.p_offset & ~PGMASK;
              uint32_t mem_page = phdr.p_vaddr & ~PGMASK;
              uint32_t page_offset = phdr.p_vaddr & PGMASK;
              uint32_t read_bytes, zero_bytes;
              if (phdr.p_filesz > 0)
                {
                  /* Normal segment.
                     Read initial part from disk and zero the rest. */
                  read_bytes = page_offset + phdr.p_filesz;
                  zero_bytes = (ROUND_UP (page_offset + phdr.p_memsz, PGSIZE)
                                - read_bytes);
                }
              else 
                {
                  /* Entirely zero.
                     Don't read anything from disk. */
                  read_bytes = 0;
                  zero_bytes = ROUND_UP (page_offset + phdr.p_memsz, PGSIZE);
                }
              if (!load_segment (file, file_page, (void *) mem_page,
                                 read_bytes, zero_bytes, writable))
                {printf("load_seg fail\n");goto done;}
              //printf("%d: load seg success\n", thread_current()->tid);
            }
          else
            goto done;
          break;
        }
    }

  /* Set up stack. */
  //printf("bef setup stack\n");
  if (!setup_stack (esp)){
    printf("set up fail\n");
    goto done;
  }
  //printf("%d: set up stack success\n", thread_current()->tid);
  /* Start address. */
  *eip = (void (*) (void)) ehdr.e_entry;

  //file_deny_write(file);

  success = true;

 done:
  /* We arrive here whether the load is successful or not. */
  //file_close (file);
  return success;
}

/* load() helpers. */

static bool install_page (void *upage, void *kpage, bool writable);

/* Checks whether PHDR describes a valid, loadable segment in
   FILE and returns true if so, false otherwise. */
static bool
validate_segment (const struct Elf32_Phdr *phdr, struct file *file) 
{
  /* p_offset and p_vaddr must have the same page offset. */
  if ((phdr->p_offset & PGMASK) != (phdr->p_vaddr & PGMASK)) 
    return false; 

  /* p_offset must point within FILE. */
  if (phdr->p_offset > (Elf32_Off) file_length (file)) 
    return false;

  /* p_memsz must be at least as big as p_filesz. */
  if (phdr->p_memsz < phdr->p_filesz) 
    return false; 

  /* The segment must not be empty. */
  if (phdr->p_memsz == 0)
    return false;
  
  /* The virtual memory region must both start and end within the
     user address space range. */
  if (!is_user_vaddr ((void *) phdr->p_vaddr))
    return false;
  if (!is_user_vaddr ((void *) (phdr->p_vaddr + phdr->p_memsz)))
    return false;

  /* The region cannot "wrap around" across the kernel virtual
     address space. */
  if (phdr->p_vaddr + phdr->p_memsz < phdr->p_vaddr)
    return false;

  /* Disallow mapping page 0.
     Not only is it a bad idea to map page 0, but if we allowed
     it then user code that passed a null pointer to system calls
     could quite likely panic the kernel by way of null pointer
     assertions in memcpy(), etc. */
  if (phdr->p_vaddr < PGSIZE)
    return false;

  /* It's okay. */
  return true;
}

/* Loads a segment starting at offset OFS in FILE at address
   UPAGE.  In total, READ_BYTES + ZERO_BYTES bytes of virtual
   memory are initialized, as follows:

        - READ_BYTES bytes at UPAGE must be read from FILE
          starting at offset OFS.

        - ZERO_BYTES bytes at UPAGE + READ_BYTES must be zeroed.

   The pages initialized by this function must be writable by the
   user process if WRITABLE is true, read-only otherwise.

   Return true if successful, false if a memory allocation error
   or disk read error occurs. */
static bool
load_segment (struct file *file, off_t ofs, uint8_t *upage,
              uint32_t read_bytes, uint32_t zero_bytes, bool writable) 
{
  ASSERT ((read_bytes + zero_bytes) % PGSIZE == 0);
  ASSERT (pg_ofs (upage) == 0);
  ASSERT (ofs % PGSIZE == 0);

  file_seek (file, ofs);
  while (read_bytes > 0 || zero_bytes > 0) 
    {
      /* Calculate how to fill this page.
         We will read PAGE_READ_BYTES bytes from FILE
         and zero the final PAGE_ZERO_BYTES bytes. */
      size_t page_read_bytes = read_bytes < PGSIZE ? read_bytes : PGSIZE;
      size_t page_zero_bytes = PGSIZE - page_read_bytes;
      
      if(!insert_STE_for_file(file, thread_current()->ST, upage, ofs, 
        page_read_bytes, page_zero_bytes, writable))
        return false;
      
      //struct STE* ste = find_ste(thread_current()->ST, upage);
      //printf("%d,%d,%d,%d\n", file_length(ste->file), ste->read_bytes, ste->zero_bytes, ste->offset);
      
      /*uint8_t *kpage = allocate_frame(thread_current(), upage, false);
      if (kpage == NULL)
        return false;

      
      if (file_read (file, kpage, page_read_bytes) != (int) page_read_bytes)
        {
          free_frame(kpage);
          return false; 
        }
      memset (kpage + page_read_bytes, 0, page_zero_bytes);

      
      if (!install_page (upage, kpage, writable)) 
        {
          free_frame(kpage);
          return false; 
        }*/

      /* Advance. */
      read_bytes -= page_read_bytes;
      zero_bytes -= page_zero_bytes;
      ofs += PGSIZE;
      upage += PGSIZE;
    }
    //printf("%x,%d\n", upage, thread_current()->tid);
  return true;
}

/* Create a minimal stack by mapping a zeroed page at the top of
   user virtual memory. */
static bool
setup_stack (void **esp) 
{
  /*uint8_t *kpage;
  bool success = false;
  kpage = allocate_frame(thread_current(), ((uint8_t *) PHYS_BASE) - PGSIZE, true);
  if (kpage != NULL) 
    {
      success = install_page (((uint8_t *) PHYS_BASE) - PGSIZE, kpage, true);
      if (success)
      {
        *esp = PHYS_BASE;
        insert_STE_for_stack(thread_current()->ST, ((uint8_t *) PHYS_BASE) - PGSIZE, kpage);
      }
      else{
        free_frame(kpage);
      }
    }
  return success;*/
  
  if (load_page_for_setup_stack()){
    *esp = PHYS_BASE;
    return true;
  }
  else
    return false;

}

/* Adds a mapping from user virtual address UPAGE to kernel
   virtual address KPAGE to the page table.
   If WRITABLE is true, the user process may modify the page;
   otherwise, it is read-only.
   UPAGE must not already be mapped.
   KPAGE should probably be a page obtained from the user pool
   with palloc_get_page().
   Returns true on success, false if UPAGE is already mapped or
   if memory allocation fails. */
static bool
install_page (void *upage, void *kpage, bool writable)
{
  struct thread *t = thread_current ();

  /* Verify that there's not already a page at that virtual
     address, then map our page there. */
  return (pagedir_get_page (t->pagedir, upage) == NULL
          && pagedir_set_page (t->pagedir, upage, kpage, writable));
}
